package ru.tsc.kirillov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.TaskRemoveByIndexRequest;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIndexListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удалить задачу по её индексу.";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIndexListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Удаление задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        getTaskEndpoint().removeTaskByIndex(new TaskRemoveByIndexRequest(getToken(), index));
    }

}
