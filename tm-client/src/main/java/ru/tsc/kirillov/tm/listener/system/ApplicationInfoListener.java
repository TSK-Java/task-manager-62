package ru.tsc.kirillov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.NumberUtil;

@Component
public final class ApplicationInfoListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getName() {
        return "info";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-i";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение информации о системе.";
    }

    @Override
    @EventListener(condition = "@applicationInfoListener.getName() == #event.name || @applicationInfoListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[INFO]");
        @NotNull final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "безлимитно" : NumberUtil.formatBytes(maxMemory);
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("Количество процесоров (ядер): " + availableProcessors);
        System.out.println("Свободно памяти: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Максимум памяти: " + maxMemoryFormat);
        System.out.println("Всего доступно памяти для JVM: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Используется памяти в JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
