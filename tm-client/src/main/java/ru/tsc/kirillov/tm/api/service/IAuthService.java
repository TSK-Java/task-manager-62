package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.UserDto;

public interface IAuthService {

    @Nullable
    UserDto registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDto check(@Nullable String login, @Nullable String password);

}
