package ru.tsc.kirillov.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.IReceiverService;

import javax.jms.*;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ReceiverService implements IReceiverService {

    @NotNull
    private static final String QUEUE_NAME = "TM_LOG";

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @Override
    @SneakyThrows
    public void init(@NotNull MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createQueue(QUEUE_NAME);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}
