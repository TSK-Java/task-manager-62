package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.kirillov.tm.api.service.dto.ITaskDtoService;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.UserIdEmptyException;

import java.util.List;

@Service
public class TaskDtoService
        extends AbstractWbsDtoService<TaskDto, ITaskDtoRepository>
        implements ITaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;
    
    @NotNull
    @Override
    protected ITaskDtoRepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    public List<TaskDto> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        repository.removeAllByProjectId(userId, projectId);
    }

    @Override
    public void removeAllByProjectList(@Nullable final String userId, @Nullable final String[] projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null || projects.length == 0) throw new ProjectNotFoundException();
        repository.removeAllByProjectList(userId, projects);
    }

}
