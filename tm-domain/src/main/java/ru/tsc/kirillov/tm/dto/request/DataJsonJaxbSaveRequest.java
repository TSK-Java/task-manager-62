package ru.tsc.kirillov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataJsonJaxbSaveRequest extends AbstractUserRequest {

    public DataJsonJaxbSaveRequest(@Nullable final String token) {
        super(token);
    }

}
