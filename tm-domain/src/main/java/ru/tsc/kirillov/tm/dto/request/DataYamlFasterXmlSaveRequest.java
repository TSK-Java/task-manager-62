package ru.tsc.kirillov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataYamlFasterXmlSaveRequest extends AbstractUserRequest {

    public DataYamlFasterXmlSaveRequest(@Nullable final String token) {
        super(token);
    }

}
